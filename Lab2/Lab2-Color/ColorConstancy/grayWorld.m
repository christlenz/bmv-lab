function [ img_out ] = grayWorld( img_in )

red = img_in(:,:,1);
green = img_in(:,:,2);
blue = img_in(:,:,3);
l_red = prctile(red(:), 99); 
l_green = prctile(green(:), 99);
l_blue = prctile(blue(:), 99);

avg_red = mean(red(:));
avg_green = mean(green(:));
avg_blue = mean(blue(:));

f = 1.7;
l_red = f * avg_red;
l_green = f * avg_green;
l_blue = f * avg_blue;

img_out = zeros(size(img_in));
img_out(:,:,1) = img_in(:,:,1) / l_red;
img_out(:,:,2) = img_in(:,:,2) / l_green;
img_out(:,:,3) = img_in(:,:,3) / l_blue;

red = img_out(:,:,1);
green = img_out(:,:,2);
blue = img_out(:,:,3);
l_red = prctile(red(:), 99) 
l_green = prctile(green(:), 99)
l_blue = prctile(blue(:), 99)



end

