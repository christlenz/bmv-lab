function [ img_out ] = whitePatchRetinex( img_in )
l_red = max(max(img_in(:,:,1)));
l_green = max(max(img_in(:,:,2)));
l_blue = max(max(img_in(:,:,3)));

img_out = zeros(size(img_in));
img_out(:,:,1) = img_in(:,:,1) / l_red;
img_out(:,:,2) = img_in(:,:,2) / l_green;
img_out(:,:,3) = img_in(:,:,3) / l_blue;
end

