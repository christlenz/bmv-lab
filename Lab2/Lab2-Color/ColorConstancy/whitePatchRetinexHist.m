function [ img_out ] = whitePatchRetinexHist( img_in )

red = img_in(:,:,1);
green = img_in(:,:,2);
blue = img_in(:,:,3);
l_red = prctile(red(:), 99); 
l_green = prctile(green(:), 99);
l_blue = prctile(blue(:), 99);

img_out = zeros(size(img_in));
img_out(:,:,1) = img_in(:,:,1) / l_red;
img_out(:,:,2) = img_in(:,:,2) / l_green;
img_out(:,:,3) = img_in(:,:,3) / l_blue;

end

