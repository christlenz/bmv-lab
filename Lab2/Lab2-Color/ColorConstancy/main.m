clear all
close all
clc

img = im2double(imread('data/non_clipping_with_light.png'));

% TODO: implement: white patch retinex algorithm
wpr = whitePatchRetinex(img);
% TODO: implement: white patch retinex algorithm using histogram information
wpr_h = whitePatchRetinexHist(img);
% TODO: implement: gray world assumption
grwrld = grayWorld(img);

figure(1)
imshow(wpr)

figure(2)
imshow(wpr_h)

figure(3)
imshow(grwrld)