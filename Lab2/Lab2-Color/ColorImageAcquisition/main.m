clear all
close all
clc

raw = im2double(imread('data/raw1.bmp'));
%rgb = imread('data/rgb1.bmp');

% TODO: create color image:
rawSize = size(raw)
imshow(raw)

% generate sub-images with only one colour type in it, all other pixels are zero
gruen = zeros(size(raw));
gruen(2:2:end, 1:2:end) = raw(2:2:end, 1:2:end);
gruen(1:2:end, 2:2:end) = raw(1:2:end, 2:2:end);

rot = zeros(size(raw));
rot(1:2:end, 1:2:end) = raw(1:2:end, 1:2:end);

blau = zeros(size(raw));
blau(2:2:end, 2:2:end) = raw(2:2:end, 2:2:end);

% Generate filter kernels
% According to the bayer pattern, at each position, the filter weights sum to 1
kernel_green = [0.2, 0.25, 0.2;
                0.25, 0.2, 0.25;
                0.2, 0.25, 0.2];
            
kernel_rb =     [0.25, 0.5, 0.25;
                 0.5, 1, 0.5;
                 0.25, 0.5, 0.25];
				 
% Convolute the sub-images with our kernels and combine them to the final RGB image         
result = zeros([size(raw), 3]);
result(:, :, 1) = imfilter(rot, kernel_rb);
result(:, :, 2) = imfilter(gruen, kernel_green);
result(:, :, 3) = imfilter(blau, kernel_rb);

imshow(result)

