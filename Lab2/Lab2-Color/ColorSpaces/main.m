clear all
close all
clc

img = im2double(imread('data/1.png'));

% TODO: transform into the following color spaces:
% - RGB
% - HSV
% - Lab
% - XYZ

hsv_img = rgb2hsv(img);
