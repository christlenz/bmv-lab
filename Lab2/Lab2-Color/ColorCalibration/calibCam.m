function [ n_img ] = calibCam( val_img, val_target, img )
% Color camera calibration
% Input:
%   val_img ... rgb color values of the image (Nx3)
%   val_target ... rgb color values of the calibration pattern (Nx3)
%   img ... original image
%
% Return:
%   n_img ... calibrated image
N = size(val_img,1); % N = number of patches
M = val_target'/val_img';

n_img = zeros(size(img));

numRows = size(img, 1);
numColumns = size(img, 2);

P = reshape(img, numRows * numColumns, 3) * M';
n_img = reshape(P, numRows, numColumns, 3);

% for k = 1:size(img, 1)
%     for j = 1:size(img, 2)
%         n_img(k, j, :) = img(k, j, :) * M';
%     end
% end

