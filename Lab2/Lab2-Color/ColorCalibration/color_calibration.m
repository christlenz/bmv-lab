clear all
close all

img = im2double(imread('colourpic.png'));

color_checker = im2double(imread('color_checker.png'));

figure, imshow(img);
title('Original image');

%% implement your own calibration method
% select colored squares:
[ c_img, r_img, val_img ] = getSquareColors( img, 3 );
[ c_target, r_target, val_target ] = getSquareColors( color_checker, 3 );

% calculate mixing coefficients
% TODO: implement the function calibCam
[ n_img ] = calibCam( val_img, val_target, img );
figure, imshow(n_img);
title('Calibrated image I');

%% compare with gray value curve fitting
img = imread('colourpic.png');
color_checker = imread('color_checker.png');

[ c_img, r_img, gray_img ] = getSquareColors( img, 6 );
[ c_target, r_target, gray_target ] = getSquareColors( color_checker, 6 );
[ a_rgb ] = grayValueCurveFitting( gray_img, gray_target, img );

a_rgb = im2double(a_rgb);

figure; imshow(a_rgb);
title('Calibrated image II');

%% compare the calibrated image with the ground truth data
% TODO
img = im2double(img);
color_checker = im2double(color_checker);

[ c_n_img, r_n_img, val_n_img ] = getSquareColors( n_img, 24);
[ c_img, r_img, val_img ] = getSquareColors( img, 24 );
[ c_target, r_target, val_target ] = getSquareColors( color_checker, 24 );

error_ourMethod = norm(val_n_img - val_target)
error_originalPicture = norm(val_img - val_target)

[ c_n_img, r_n_img, val_n_img ] = getSquareColors( a_rgb, 24);

error_GVCF = norm(val_n_img - val_target)