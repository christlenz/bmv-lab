%
% Cross Ratio Test
% 
addpath('../Helpers/');
addpath('../Helpers/CalibToolBox');

close all

% load image and extract cornsers:
img = imread('images/tele_5.bmp');
fprintf('Select the four points A,B,C,D !\n');
pts=markCorners(img,4);

cr=CrossRatio(pts)
