function [cr]=CrossRatio(points)
% function [cr]=CrossRatio(points) 
%
% Determine the cross ratio of 4 points in 2D,
% where 'points' is a 2x4 matrix.
%

%
% $Id: CrossRatio.m,v 1.1 2005/04/25 05:34:46 brandner Exp $
%

% TODO: compute the cross ratio

ac = norm(points(:,3) - points(:,1));
bc = norm(points(:,3) - points(:,2));
ad = norm(points(:,4) - points(:,1));
bd = norm(points(:,4) - points(:,2));

cr = (ac / bc) / (ad / bd);

