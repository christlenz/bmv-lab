%
% Panorama stitching
%
addpath('../Helpers/');
addpath('../Helpers/CalibToolBox');

% load first image and extract corners
img1 = imread('./images/Wide_left.bmp');
fprintf('Select the four points A,B,C,D !\n');
pts1=markCorners(img1,4);

% load second image and extract corners
img2 = imread('./images/Wide_right.bmp');
fprintf('Select the four points A,B,C,D !\n');
pts2=markCorners(img2,4);


PanoramaStitching(img1, img2, pts1, pts2);