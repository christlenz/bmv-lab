%
% Autofocus
%

% directory that contains the image sequence
img_dir = './images/';
% type of the image (e.g. jpg)
img_type = '.bmp';
img_names = dir([img_dir '/*' img_type]);

fft_log_sums = zeros(size(img_names));
gradient_sum = zeros(size(img_names));

for idx=1:size(img_names)
    % name of the first image
    img_name = img_names(idx).name;
    img = imread([img_dir '/' img_name]);
    
    gray_img = rgb2gray(img);
    
    %fft
    fft_result = fft2(gray_img);
    fft_log = (abs(fft_result));
    fft_log_sums(idx) = sum(sum(fft_log));%sum(sum(fft_log,1),2);
    
    %gradient
    [gradient,~] = imgradient(gray_img);
    gradient_sum(idx) = sum(sum(gradient));
   
    disp([img_name]);
end

%%

fft_log_sums = fft_log_sums / max(fft_log_sums)
gradient_sum = gradient_sum / max(gradient_sum)

focus_idx_fft = find( fft_log_sums == max(fft_log_sums))
focus_idx_grad = find( gradient_sum == max(gradient_sum))


img_name = img_names(focus_idx).name;
img = imread([img_dir '/' img_name]);
figure(1)
imshow(img)

figure(2);
hold on
stem(fft_log_sums)
stem(gradient_sum, 'r')
legend('fft', 'gradient')

