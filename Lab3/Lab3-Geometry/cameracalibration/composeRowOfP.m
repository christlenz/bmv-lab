function A_i = composeRowOfP(x_hom, X_hom)
    A_i = zeros(2,12);
    A_i(1,5:8) = -x_hom(3) * X_hom';
    A_i(1,9:12) = x_hom(2)* X_hom';
    A_i(2,1:4) = x_hom(3) * X_hom';
    A_i(2,9:12) = -x_hom(1) * X_hom';
end