clear all
close all

addpath('../Helpers/');
addpath('../Helpers/CalibToolBox');

% SETTINGS:
img_type = '*.JPG';
img_dir = './';
wrld_fname = 'wrld_point_coords.mat';

% LOAD WRLD POINTS
load(wrld_fname);
pt_w_ = [PT; ones(1,size(PT,2))];

% EXTRACT IMAGE MEASUREMENTS
fnames = dir([img_dir img_type]);
N = size(fnames,1);
pt_i_ = ones(3, N);

for i=1:N
    img = im2double(imread([img_dir, fnames(i).name]));
    pt_i_(1:2,i) = markCorners(img,1);    
end

% NORMALIZE POINTS
[pt_i_n, T] = normalizePoints( pt_i_ );
[pt_w_n, U] = normalizePoints( pt_w_ );

%%
% =========================================================================
% TODO: build A matrix:
% =========================================================================
A = zeros(2*N,12);
for i=1:N
   row_idx = (i-1) * 2 + 1;
   A(row_idx:row_idx+1,:) = composeRowOfP(pt_i_n(:,i), pt_w_n(:,i)); 
end

% SOLVE LIN SYSTEM
[~,~,V] = svd(A);
P_n = V(:,end);
P_n = reshape(P_n,4,3)';
P = T^(-1)*P_n*U;

% =========================================================================
% TODO: decompose P matrix: P = K [R | t]
% hint: you can use the provided rq function
% =========================================================================

M = P(:,1:3)
minusMTimesCTilde = P(:,4)
[R,K] = qr(M)
C_tilde = - M \ minusMTimesCTilde

K_normalized = K / K(3,3)

%% plausibility check
norm_R = norm(R)
RtimesRtranspose = R * R'
