% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly excecuted under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 1623.420560851625900 ; 1622.771234626712800 ];

%-- Principal point:
cc = [ 656.349442516768820 ; 492.522798853726900 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ -0.189175450922267 ; 0.053039905127819 ; -0.003086615980043 ; 0.003423646335954 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 5.772293033744269 ; 5.627508456703414 ];

%-- Principal point uncertainty:
cc_error = [ 13.933566758255113 ; 10.086533141949056 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.043011436707285 ; 0.445060760262805 ; 0.001393644264227 ; 0.001538293793408 ; 0.000000000000000 ];

%-- Image size:
nx = 1280;
ny = 1024;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 12;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ -1.868373e+00 ; -1.973832e+00 ; 7.530207e-01 ];
Tc_1  = [ -2.050217e+01 ; -3.002785e+01 ; 2.213651e+02 ];
omc_error_1 = [ 7.119079e-03 ; 5.382208e-03 ; 9.978900e-03 ];
Tc_error_1  = [ 1.896503e+00 ; 1.374113e+00 ; 7.427206e-01 ];

%-- Image #2:
omc_2 = [ -1.725913e+00 ; -1.584531e+00 ; -7.298852e-01 ];
Tc_2  = [ -4.252779e+01 ; -2.221472e+01 ; 1.757869e+02 ];
omc_error_2 = [ 4.762517e-03 ; 7.174731e-03 ; 9.468490e-03 ];
Tc_error_2  = [ 1.515785e+00 ; 1.104751e+00 ; 8.002857e-01 ];

%-- Image #3:
omc_3 = [ -2.120038e+00 ; -2.139479e+00 ; 1.286259e-01 ];
Tc_3  = [ -4.260696e+01 ; -2.998025e+01 ; 1.665803e+02 ];
omc_error_3 = [ 6.199695e-03 ; 6.032959e-03 ; 1.237577e-02 ];
Tc_error_3  = [ 1.436184e+00 ; 1.046593e+00 ; 7.280138e-01 ];

%-- Image #4:
omc_4 = [ 1.344761e+00 ; 2.414347e+00 ; -1.182375e+00 ];
Tc_4  = [ -1.378782e+01 ; -3.421811e+01 ; 2.169545e+02 ];
omc_error_4 = [ 3.435831e-03 ; 8.283829e-03 ; 1.060081e-02 ];
Tc_error_4  = [ 1.863933e+00 ; 1.354882e+00 ; 6.868996e-01 ];

%-- Image #5:
omc_5 = [ 2.239758e+00 ; 1.023009e+00 ; -3.565676e-01 ];
Tc_5  = [ -5.219373e+01 ; -9.013019e+00 ; 1.846104e+02 ];
omc_error_5 = [ 5.861984e-03 ; 5.514544e-03 ; 1.049562e-02 ];
Tc_error_5  = [ 1.590621e+00 ; 1.160191e+00 ; 8.215977e-01 ];

%-- Image #6:
omc_6 = [ -1.778406e+00 ; -1.316064e+00 ; 3.093085e-01 ];
Tc_6  = [ -4.505576e+01 ; -1.966302e+01 ; 1.871805e+02 ];
omc_error_6 = [ 6.291328e-03 ; 6.271966e-03 ; 8.604853e-03 ];
Tc_error_6  = [ 1.608828e+00 ; 1.170765e+00 ; 7.048131e-01 ];

%-- Image #7:
omc_7 = [ 2.018527e+00 ; 1.882005e+00 ; 1.047295e+00 ];
Tc_7  = [ -2.972040e+01 ; -2.300261e+01 ; 1.345343e+02 ];
omc_error_7 = [ 8.600460e-03 ; 4.735053e-03 ; 1.010351e-02 ];
Tc_error_7  = [ 1.165155e+00 ; 8.485467e-01 ; 6.655015e-01 ];

%-- Image #8:
omc_8 = [ 1.689686e+00 ; 1.353063e+00 ; -8.132309e-01 ];
Tc_8  = [ -5.357884e+01 ; -4.562081e+00 ; 2.059929e+02 ];
omc_error_8 = [ 4.971845e-03 ; 7.106376e-03 ; 8.897459e-03 ];
Tc_error_8  = [ 1.768902e+00 ; 1.298242e+00 ; 8.215417e-01 ];

%-- Image #9:
omc_9 = [ -1.721922e+00 ; -1.935396e+00 ; 6.351459e-01 ];
Tc_9  = [ -4.360238e+01 ; -3.422857e+01 ; 2.125635e+02 ];
omc_error_9 = [ 7.013989e-03 ; 6.188927e-03 ; 9.617876e-03 ];
Tc_error_9  = [ 1.829022e+00 ; 1.336719e+00 ; 7.965094e-01 ];

%-- Image #10:
omc_10 = [ 1.915248e+00 ; 1.618108e+00 ; 9.913589e-01 ];
Tc_10  = [ -2.739056e+01 ; -2.235643e+01 ; 1.544952e+02 ];
omc_error_10 = [ 8.137283e-03 ; 5.181046e-03 ; 9.481335e-03 ];
Tc_error_10  = [ 1.333647e+00 ; 9.682840e-01 ; 7.348242e-01 ];

%-- Image #11:
omc_11 = [ 1.211674e+00 ; 2.361040e+00 ; -1.154347e+00 ];
Tc_11  = [ -1.506713e+01 ; -4.214362e+01 ; 2.413039e+02 ];
omc_error_11 = [ 3.830630e-03 ; 8.490036e-03 ; 1.021185e-02 ];
Tc_error_11  = [ 2.081345e+00 ; 1.510221e+00 ; 7.909912e-01 ];

%-- Image #12:
omc_12 = [ 1.959090e+00 ; 1.196648e+00 ; -4.652241e-01 ];
Tc_12  = [ -5.753335e+01 ; -1.552703e+01 ; 2.251047e+02 ];
omc_error_12 = [ 5.421711e-03 ; 6.496318e-03 ; 9.664303e-03 ];
Tc_error_12  = [ 1.936376e+00 ; 1.416563e+00 ; 9.707753e-01 ];

