function A_i = composeARowFromPoint(x, x_prime)
    A_i = zeros(1, 9);
    A_i(1,1) = x_prime(1) * x(1);
    A_i(1,2) = x_prime(1) * x(2);
    A_i(1,3) = x_prime(1);
    A_i(1,4) = x_prime(2) * x(1);
    A_i(1,5) = x_prime(2) * x(2);
    A_i(1,6) = x_prime(2);
    A_i(1,7) = x(1);
    A_i(1,8) = x(2);
    A_i(1,9) = 1;