close all
%
% Calculate Fundamental Matrix
%
addpath('../Helpers/');
addpath('../Helpers/CalibToolBox');
USE_NORMALIZED_ALGO = true;

% load first image and extract corners
img_left = imread('img_left.jpg');
fprintf('Select the eight points!\n');
pts1=markCorners(img_left,8);
% 
img_right = imread('img_right.jpg');
fprintf('Select the eight points!\n');
pts2=markCorners(img_right,8);

%%
N = length(pts1);
% create arrays with homogenous coordinates:
pts1_hom = [pts1; ones(1, N)]; pts1_hom_nonnorm = pts1_hom;
pts2_hom = [pts2; ones(1, N)]; pts2_hom_nonnorm = pts2_hom;

% normalize the homogenous coordinates:
if USE_NORMALIZED_ALGO
    [pts1_hom, T1] = normalize_points(pts1_hom);
    [pts2_hom, T2] = normalize_points(pts2_hom);
end

%%
% TODO: 1) setup linear equation system:
% -------------------------------------------------------------------------
% ...
% ..
% .
A = zeros(N,9);
for i=1:N
    A(i,:) = composeARowFromPoint(pts1_hom(:,i), pts2_hom(:,i));
end

[U_A, S_A, V_A] = svd(A);

f_row = V_A(:,end)

F = reshape(f_row, 3, 3);

% F check
residuals = zeros(1,8);
for i=1:8
    residuals(i) = pts1_hom(:,i)' * F * pts2_hom(:,i);
end

rank_F = rank(F)

% =========================================================================

  
% TODO: 2) impose rank 2 constraint:
% -------------------------------------------------------------------------
% ...
% ..
% .
[U_F, S_F, V_F] = svd(F);
S_F
S_F(3,3) = 0;

F_hat = U_F * S_F * V_F';

% F check
residuals_hat = zeros(1,8);
for i=1:8
    residuals_hat(i) = pts1_hom(:,i)' * F_hat * pts2_hom(:,i);
end

rank_F_hat = rank(F_hat)
% =========================================================================

if USE_NORMALIZED_ALGO
    F = T2'*F*T1;
    F_hat = T2'*F_hat*T1;
end

% TODO: 3) show stereo pair and plot the epipolar lines:
%          (complete and use the function 'drawEpipolarLine')
% -------------------------------------------------------------------------
% ...
% ..
% .
% figure(1), imshow(img_left), figure(2),imshow(img_right)
drawEpipolarLine(F_hat, pts1_hom_nonnorm, pts2_hom_nonnorm)
% =========================================================================